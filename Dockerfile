FROM debian:bookworm

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
                    curl \
                    apt-transport-https \
                    lsb-release \
                    ca-certificates \
                    lsb-release \
                    gnupg && \
    curl https://nginx.org/keys/nginx_signing.key | apt-key add - && \
    echo "deb http://nginx.org/packages/debian $(lsb_release -sc) nginx" >> /etc/apt/sources.list.d/nginx.list && \
    apt-get update -y && \
    echo ${PHP_VERSION} && \
    apt-get install -y --no-install-recommends nginx && \
    rm -r /var/lib/apt/lists/* && \
    mkdir /etc/nginx/cache/ && \
    mkdir /etc/nginx/cache/fastcgi/ && \
    mkdir /etc/nginx/cache/proxy/ && \
    rm /etc/nginx/conf.d/default.conf

COPY files/nginx.conf /etc/nginx/nginx.conf
COPY files/fastcgi_params.conf /etc/nginx/fastcgi_params.conf
COPY files/phalcon.conf /etc/nginx/conf.d/phalcon.conf

WORKDIR /var/www

# Add Dockerfile to the container
# It helps for debugging & maintainability + is considered good practice
COPY Dockerfile /

CMD ["/usr/sbin/nginx", "-c", "/etc/nginx/nginx.conf"]
